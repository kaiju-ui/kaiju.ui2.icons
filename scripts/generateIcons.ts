import fs from "fs";
import path from "path";

const iconsDir = path.resolve("src/icons");
const outputFile = path.resolve("src/index.tsx");

const iconFiles = fs.readdirSync(iconsDir).filter(file => file.endsWith(".tsx"));

const imports = iconFiles
    .map(file => {
        const name = file.replace(".tsx", "");
        let import_name = ""
        if (parseInt(name.slice(0, 1))){
            import_name = "{ default as Icon" + name + "}"
        } else {
            import_name = "{ default as " + name + "Icon }"
        }
        return `export ${import_name} from './icons/${name}.js';`;
    })
    .join("\n");

fs.writeFileSync(outputFile, `${imports}`);

console.log(`✅ Generated file with icon imports: src/icons.tsx`);