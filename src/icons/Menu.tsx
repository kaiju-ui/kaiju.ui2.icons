import React from 'react'

export default function (props: any) {
    return (
        <svg {...props} version="1.0" xmlns="http://www.w3.org/2000/svg" className="rs-icon" fill="currentColor"
             viewBox="0 0 512.000000 512.000000"
             preserveAspectRatio="xMidYMid meet">

            <g transform="translate(0.000000,512.000000) scale(0.100000,-0.100000)"
               fill="#000000" stroke="none">
                <path d="M980 4034 c-173 -75 -161 -342 18 -394 26 -8 505 -10 1584 -8 l1546
3 44 30 c138 97 118 306 -34 370 -33 13 -210 15 -1580 15 -1356 -1 -1547 -3
-1578 -16z"/>
                <path d="M980 2754 c-173 -75 -161 -342 18 -394 26 -8 381 -10 1158 -8 l1120
3 37 25 c21 14 50 43 65 64 24 35 27 49 27 117 0 71 -3 81 -31 120 -18 24 -51
54 -75 66 l-42 23 -1121 0 c-979 -1 -1125 -3 -1156 -16z"/>
                <path d="M980 1474 c-167 -72 -165 -318 2 -389 33 -13 210 -15 1578 -15 1368
0 1545 2 1578 15 169 71 169 319 0 390 -33 13 -210 15 -1580 15 -1356 -1
-1547 -3 -1578 -16z"/>
            </g>
        </svg>


    )
}